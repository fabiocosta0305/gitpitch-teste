## Github Pages para Pessoas Comuns

 Fábio Emilio Costa

<fabiocosta0305@gmail.com>

<http://fabiocosta0305.gitlab.io>

---

## O Autor

![Fábio](assets/presentations/Fabio.jpg)

+++

![RPG](assets/presentations/Fabio-RPG.jpg)

+++

![Rugby](assets/presentations/Fabio-Rugby.jpg)

+++

![Doctor Who](assets/presentations/FabioTARDIS.jpg)

+++

![Cosplayer](assets/presentations/FabioCosplayRogerRabbit.jpg)

+++

Porque pagar mico também faz parte da vida

+++

- Linux   
- RPG     |
- Cosplay |
- Rugby   |
- Go      |
- Anime   |
- Xadrez  |
- Indycar |
- Github  |
- PHP     |

+++

Esse sou eu!

---

Apenas uma introdução!

--- 

Manter um blog é complicado ...

+++

+ Custos
+ Disponibilidade
+ Recursos
+ Otimizações
+ SEO

---

... e demanda uma estrutura complicada e potencialmente desnecessária.

+++

+ Wordpress
+ PHP
+ MySQL
+ HTML
+ Módulos
+ Plugins
+ ...

---

## Voltando ao primórdio da Internet...

+++

![Inicio](assets/presentations/Inicio.jpg)

+++

... OK, não ***TÃO*** ao primórdio da Internet!

---

Navegadores fazem a mesma coisa desde sempre

+++

![Mosaic](assets/presentations/Mosaic.jpg)

+++

![Inicio](assets/presentations/Inicio.jpg)

+++

![Chrome](assets/presentations/Chrome.png)

+++

+ Dados em texto HTML
+ Imagens
+ Renderiza (Constroi) a página

---

## HTML é ***SEMPRE*** HTML

+++

```
<html>
   <head>
      <title> HOW ARE YOU, GENTLEMEN! </title>
   </head>
   <body>
      <h1> ALL YOUR BASE ARE BELONG TO US! </h1>
   </body>
</html>
```

---

Por que existem Wordpress, Blogger, Tumblr, Medium, etc...?

+++

HTML é chato!

+++

Subir páginas era complicado

+++

Quase instantâneo

+++

***Problema:*** Infraestrutura

---

## Alternativa: Github Pages

+++

Mantida no serviço Github

+++

Usa Markdown para os posts

+++

Renderizado pelo Jekyll

+++

Git para a publicação

---

# PARA TUDO!!!!

+++


![Rocky](assets/presentations/Rocky.jpg)

Que _Flarka_ esse cara tá falando?!?!?!?!?!?

---

### Git - Sistema de Controle de Versões

+++

Usado em projetos de programação

+++

Caso tudo dê errado, você pode voltar atrás

+++

Decentralizado: várias pessoas podem trabalhar em um projeto

+++

Repositórios: você trabalha no seu sistema sem quebrar tudo

+++

Performático, usado por vários projetos e empresas

+++

Multiplataforma

+++

![Git](assets/presentations/Git.jpg)

---

### Github.com - Site de repositórios Git social

+++

Site que funciona como repositório remoto para Git

- Backup em nuvem |
- Forks |
- _Pull Requests_ |
   - Fiz algo e quero que você avalie |
- _Issues_ |
   - Faz isso pra mim? |

+++

Pode ser usado para qualquer tipo de conteúdo, não apenas código

+++ 

Existem serviços similares com basicamente os mesmos recursos

- [Gitlab](http://Gitlab.com)
- [BitBucket](http://Bitbucket.com)

+++

Github Pages - Pode ser usado para blogs, documentação de projetos e afins...

+++

![Github](assets/presentations/Github.png)

--- 

### Jekyll

+++

Gerador de Páginas Estáticas

+++

Ao invés de processar a origem vez por vez, processa de uma vez só e publica

+++

- Baseado em temas 
    - Temas representam a formatação básica de um post |

+++

Economia de Recursos - Só refaz quando alterado

+++

Pode publicar em qualquer lugar

+++

Multiplataforma (apenas com alguns problemas em Windows)

---

### Markdown

+++

Linguagem de Marcação de Texto

- HTML |
- LaTeX |
- WikiWiki |
- Markdown |

+++

Símbolos no texto indicam a formatação

+++

Simples e eficiente

+++

Poderosa e flexível, usada em muitos sistemas

+++

```
# HOW ARE YOU GENTLEMEN!



## ALL YOUR BASE ARE BELONG TO US!
```

+++

# HOW ARE YOU GENTLEMEN!

## ALL YOUR BASE ARE BELONG TO US!

---

### Markdown 101

+++

### Formatação

+++

- \_Itálico\_ (ou \*Itálico\*) - _Itálico_
- \_\_Negrito\_\_ (ou \*\*Negrito\*\*) - __Negrito__
- \_\_\_Negrito Itálico\_\_\_  - ___Negrito Itálico___
   - ou \*\*\*Negrito Itálico\*\*\* 
   - ou ainda \*\_\_Negrito Itálico\_\_\*
- \~\~Riscado\~\~ - ~~Riscado~~ 

+++

### Títulos

+++

# Cabeçalho 1

`# Cabeçalho 1`

+++

## Cabeçalho 2

`## Cabeçalho 2`

+++

### Cabeçalho 3

`### Cabeçalho 3`

+++

E assim por diante, cada `#` no início adiciona um nível ao cabeçalho

+++

### Listas

+++

1. Item 1
2. Item 2
3. Item 3

```
1. Item 1
2. Item 2
3. Item 3
```

+++

1. Item 1
1. Item 2
1. Item 3

```
1. Item 1
1. Item 2
1. Item 3
```
+++

+ Um item
+ Outro item
+ Mais um item

```
+ Um item
+ Outro item
+ Mais um item
```

+++

- Um item
- Outro item
- Mais um item

```
- Um item
- Outro item
- Mais um item
```

+++

### Links

+++

[Clickando aqui](http://google.com) você vai para o Google

[Clickando aqui][google] você também vai para o Google

```
[Clickando aqui](http://google.com) você vai para o Google
[Clickando aqui][google] você também vai para o Google

[google]: http://google.com
```

[google]: http://google.com

---

## Como tudo isso se ajunta?

+++

_Posts_ criados em Markdown...

+++

...versionados com Git...

+++

...enviados ao Github...

+++

...e renderizados pelo Jekyll no Github Pages

---

## Criando seu _blog_ no Github Pages

(...passos similares para outros serviços)

+++

Instale

+ Git
+ Editor de Texto (Notepad, EMAC, VI, Atom, Notepad++...)
  + Não confundir com _Processador de Texto_ (Word, LibreOffice Writer...)
+ Jekyll (opcional, para Teste)

+++

Crie uma conta no Github

+++

Crie um repositório especial esperado pelo Github pages

`<seu_login>.github.io`

+++

Clone (copie) ele localmente usando o comando


`git clone http://github.com/<seu_login>/<seu_login>.github.io.git`

+++

Seu blog estará (localmente) na pasta `<seu_login>.github.io`

+++

Obtenha (ou crie) um ___tema___ do Jekyll

<http://jekyllthemes.org>

+++

Alguns temas ___não são compatíveis com o Github___

---

### Meu primeiro post

+++

Os post vão na pasta `_post` dentro da pasta do seu repositório

+++

O nome do arquivo deve ser 

`AAAA-MM-DD-Assunto.md`

+ AAAA – Ano
+ MM – Mês
+ DD – Dia
+ Assunto.md – um nome que indique sobre o que se trata o assunto do post. A extensão .md é obrigatória e indica arquivo em Markdown

+++?code=first.md&lang=markdown

@[1-13](_Frontmatter_ com informações importantes sobre o seu post)
@[2](Título do post)
@[3](Subtítulo)
@[4](_Layout_ a ser usado no post. Alguns temas podem ter múltiplos _layouts_, o que permite uma apresentação mais rica)
@[5-6](Categorias relacionadas)
@[7-11](_Tags_ do post)
@[12](uma informação específica do tema, se deve utilizar um cabeçalho ou não - sempre cheque a documentação do tema)
@[14-17](Texto do post, formatado em _markdown_)

---

### _Frontmatter_

+++

Para informações adicionais usadas pelo Jekyll e/ou pelo tema

+++

Depende do tema

+++

Usa a linguagem YAML

```yaml

Nível 1
   - Nível 2
   - Nível 2
     Extra 2
     - Nível 3
   - Nível 2
Nível 1
```
+++

#### Informações Comuns no _Frontmatter_

+++

`title` – Título do post

+++

`author` – autor

+++

`date` (opcional) – data da publicação

- Formatação AAAA-MM-DD HH:MM +ZZZZ 
   - AAAA – Ano
   - MM – Mês
   - DD – Dia
   - HH:MM - Horário (24 horas)
   - +ZZZZ - Fuso horário (`+0300` ou `+0200` para Brasil normalmente)

+++

`layout` – que tipo de modelo o Jekyll tem que usar  Um mesmo tema ou site pode ter vários layouts

+++

`categories` – Categorias do post

+++

`tags` – Tags do post

---

### Publicando

+++

Adicionado post ao repositório local - `git add <arquivo>`

– Em caso de vários, `git add -A` pode ser mais rápido

+++

Dando _commit_ (confirmando adição e alteração do arquivo) – `git commit -am <motivo>`

- Motivo serve para pesquisa no log

+++

Enviando para o Github -  `git push origin master`

- `origin` – Repositório remoto padrão
- `master` – Repositório (ou melhor, ramo do repositório) local a ser sincronizado

---

### Exemplo bem completo

+++?code=assets/presentations/2016-06-30-MesasPredestinadas0-Piloto.md&lang=markdown

@[1-36](_Frotmatter_)
@[2](Título do Post)
@[3](Informações adicionais - baseadas no tema)
@[4](Data de publicação do post)
@[5](_layout_ utilizado)
@[6-8](Categorias)
@[9-13](_Tags_)
@[15-24](Informações específicas do tema para gerar informações relativas a um _podcast_)
@[26-28](Linha do tempo para ser gerada no post)
@[29-35](Créditos de trilha sonora - obrigatório para o _podcast_)
@[37-45](_Post_)
@[37-42](Texto do _post_)
@[43-45](Links)

+++

![exemplo](assets/presentations/Inicio.png)

---

### Temas

+++

O segredo do Jekyll

+++

Renderizam o conteúdo dos markdown em HTML, conforme a configuração

+++

Altamente configurável

+++

___Problema:___ Exige conhecimento técnico especializado

+++

___Solução:___ Sites de Temas como o <https://jekyllthemes.org>

---

### Combinação poderosa

+++

Manutenção simples: 

Problemas podem ser resolvidos usando o `git` para voltar atrás (`rollback`) com alterações malfeitas
+++

Múltiplos usuários:

Basta criar os arquivos no _posts corretamente e enviar, e um administrador aceita depois

+++

Consistente:

Não é atualizado no site se tiver algo realmente ruim

---

# Perguntas?

---

### Tópicos avançados

+++

Estudar Jekyll/Liquid para customização do tema 


+++

_Markdown_ avançado, em especial Tabelas

+++

Git

- Consultas de modificações (`log`)
- Reverter atualizações ruins (`reset`)
- Criar pontos de retorno para situações boas (`branch`)
- Uso de múltiplos pontos remotos (`remote`)

--- 

# Obrigado
